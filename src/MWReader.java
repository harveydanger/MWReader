/**
 * Created by sukhov on 7/19/2016.
 */

import processing.core.PApplet;
import processing.core.PFont;
import neurosky.*;



public class MWReader extends PApplet {
    public ThinkGearSocket mindWaveSocket;
    public int attention=10;
    public int meditation=10;
    public int delta=0, theta = 0, low_alpha =0, high_alpha = 0, low_beta =0, high_beta = 0, low_gamma=0, mid_gamma=0;
    public PFont font;
    public boolean DEBUG = false;
    public int poorSignal =0;

    /// connection and data aquiring
    public void attentionEvent(int attentionLevel) // obtain attention
    {
        if(DEBUG)
        {
            println("Attention Level: " + attentionLevel);
        }
        attention = attentionLevel;
    }
    public void poorSignalEvent(int sig) //obtain level of signal
    {
        this.poorSignal = sig;
        if(DEBUG)
        {
            println("SignalEvent " + sig);
        }
    }
    public void meditationEvent(int meditationLevel) // obtain meditation
    {
        if(DEBUG)
        {
            println("Meditation Level: " + meditationLevel);
        }
        meditation = meditationLevel; // settle variable
    }

    public void blinkEvent(int blinkStrength)  // obtain blink event
    {
        if(DEBUG)
        {
            println("blinkStrength: " + blinkStrength);
        }
    }

    public void eegEvent(int delta, int theta, int low_alpha, int high_alpha, int low_beta, int high_beta, int low_gamma, int mid_gamma) // obtain waves from device
    {
        this.delta = delta;
        this.theta = theta;
        this.low_alpha = low_alpha;
        this.high_alpha = high_alpha;
        this.low_beta = low_beta;
        this.low_gamma = low_gamma;
        this.mid_gamma = mid_gamma;
        this.high_beta = high_beta;

        if (DEBUG) {
            println("delta Level: " + delta);
            println("theta Level: " + theta);
            println("low_alpha Level: " + low_alpha);
            println("high_alpha Level: " + high_alpha);
            println("low_beta Level: " + low_beta);
            println("high_beta Level: " + high_beta);
            println("low_gamma Level: " + low_gamma);
            println("mid_gamma Level: " + mid_gamma);

        }
    }

    /// <--connection

    public static void main(String _args[]) // entrance that processing capture
    {
    //    PApplet.main(new String[] { MWReader.class.getName() });
    }

    // PROCESSING MAIN METHODS --> SETUP
    public void setup() {
        size(600,600);
        ThinkGearSocket mindWaveSocket = new ThinkGearSocket(this);
        try
        {
            mindWaveSocket.start();
        }
        catch (Exception e)
        {
            println("Is ThinkGear running??");
        }
        smooth();
        font = createFont("Verdana",18);
        textFont(font);
    }
    // PROCESSING MAIN METHODS --> DRAW
    public void draw() {

        if(DEBUG)
        {
            text("Current theta wave value is: " + theta,10,70);
            text("Current delta wave value is: " + delta,10,90);
            text("Current low alpha wave value is: " + low_alpha,10,110);
            text("Current low beta wave value is: " + low_beta,10,130);
            text("Current low gamma wave value is: " + low_gamma,10,150);
            text("Current mid gamma wave value is: " + mid_gamma,10,170);
            text("Current high alpha wave value is: " + high_alpha,10,190);
            text("Current high beta wave value is: " + high_beta,10,210);

        }
        //background(0,0,0,50);
        fill(0, 0,0, 255);
        noStroke();
        rect(0,0,120,80);
        fill(0, 0,0, 10);
        noStroke();
        rect(0,0,width,height);
        fill(0, 116, 168);
        stroke(0, 116, 168);
        text("Attention: "+attention, 10, 30);
        noFill();
        ellipse(width/2,height/2,attention*3,attention*3);
        fill(209, 24, 117, 100);
        noFill();
        text("Meditation: "+meditation, 10, 50);
        stroke(209, 24, 117, 100);
        noFill();
        ellipse(width/2,height/2,meditation*3,meditation*3);
        if(poorSignal>0){println("Signal is poor " + poorSignal);};

    }
    // PROCESSING MAIN METHODS --> STOP
    public void stop() // kill all processes
    {
        mindWaveSocket.stop();
        super.stop();
    }













}

